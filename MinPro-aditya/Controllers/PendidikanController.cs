﻿using MinPro.Repository;
using MinPro.ViewModel;
using MinPro.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MinPro_aditya.Controllers
{
    public class PendidikanController : Controller
    {
        // GET: Pendidikan
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListPendidikan()
        {

            return PartialView("_List");
        }

        public ActionResult AddPendidikan()
        {
            return PartialView("_Create");
        }

        public ActionResult EditPendidikan()
        {
            return PartialView("_Edit");
        }

        public ActionResult DeletePendidikan()
        {

            return PartialView("_Delete");
        }

        public ActionResult SavePendidikan(x_riwayat_pendidikan x_riwayat_pendidikan)
        {
            x_riwayat_pendidikan.is_delete = false;
            x_riwayat_pendidikan.created_by = 1;
            x_riwayat_pendidikan.created_on = DateTime.Now;
            PendidikanRepo riwayat_pendidikanRepo = new PendidikanRepo();
            if (riwayat_pendidikanRepo.Save(x_riwayat_pendidikan))
            {
                return Json(new { status = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }

        public ActionResult SaveEditPendidikan(x_riwayat_pendidikan x_riwayat_pendidikan)
        {
            x_riwayat_pendidikan.is_delete = false;
            x_riwayat_pendidikan.created_by = 1;
            x_riwayat_pendidikan.created_on = DateTime.Now;
            PendidikanRepo PendidikanRepo = new PendidikanRepo();

            if (PendidikanRepo.SaveEdit(x_riwayat_pendidikan))
            {
                return Json(new { status = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }

        public ActionResult SaveDeletePendidikan(isDeletedOrNOt x_riwayat_pendidikan)
        {
            //x_riwayat_pendidikan.delete_by = 1;
            //x_riwayat_pendidikan.delete_on = DateTime.Now;
            //x_riwayat_pendidikan.is_delete = true;
            x_riwayat_pendidikan.is_delete = true;
            //x_riwayat_pendidikan.create_by = 1;
            //x_riwayat_pendidikan.create_on = DateTime.Now;
            PendidikanRepo PendidikanRepo = new PendidikanRepo();
            //x_riwayat_pendidikan.is_delete = true;

            if (PendidikanRepo.delete2(x_riwayat_pendidikan))
            {
                return Json(new { status = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
            //return View();
        }

        public ActionResult GetAll()
        {
            PendidikanRepo PendidikanRepo = new PendidikanRepo();
            List<PendidikanViewModel> x_riwayat_pendidikanModel = PendidikanRepo.GetAll();
            return Json(x_riwayat_pendidikanModel, JsonRequestBehavior.AllowGet);

        }
    }
}