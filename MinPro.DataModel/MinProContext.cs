    using MinPro.ViewModel;
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;

namespace MinPro.DataModel

{
    public  class MinProContext : DbContext
    {
        public MinProContext(): base("name=MinProContext")
        {
            Database.SetInitializer<MinProContext>(null);
        }

        public virtual DbSet<x_biodata> x_biodata { get; set; }
        public virtual DbSet<x_education_level> x_education_level { get; set; }
        public virtual DbSet<x_keahlian> x_keahlian { get; set; }
        public virtual DbSet<x_keluarga> x_keluarga { get; set; }
        public virtual DbSet<x_rencana_jadwal> x_rencana_jadwal { get; set; }
        public virtual DbSet<x_rencana_jadwal_detail> x_rencana_jadwal_detail { get; set; }
        public virtual DbSet<x_riwayat_pelatihan> X_riwayat_pelatihan { get; set; }
        public virtual DbSet<x_riwayat_pendidikan> x_riwayat_pendidikan { get; set; }
        public virtual DbSet<x_schedule_type> x_schedule_type { get; set; }
        public virtual DbSet<x_time_period> x_time_period { get; set; }
        public virtual DbSet<x_undangan> x_undangan { get; set; }
        public virtual DbSet<x_undangan_detail> x_undangan_detail { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
